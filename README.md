config-files
============
A place for configuration files and settings.

To automatically install all of the files, just type

    $ make install

and this will create all of the required symbolic links in your home directory.
